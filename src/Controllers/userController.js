const { UserModel } = require('../Models/userModel')
const { MovieModel } = require('../Models/movieModel')
const { RateModel } = require('../Models/ratingModel')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Joi = require('joi');

/*-------------------------------cc
+++++++++++++ user signup ++++++++++++
--------------------------------*/

exports.signup = async(req, res) => {
	    
	try {
		console.log("req.body==============", req.body)
			let {name,password, email} = req.body;
		    let created_on = Math.round((new Date()).getTime());
		    const schema = Joi.object().keys({
				name : Joi.string().required().error(e => 'name require'),
				password : Joi.string().required().error(e => 'password require'),
				email: Joi.string().required().error(e => 'email require'),
        	})
        	const result = Joi.validate(req.body, schema, { abortEarly: true });
	        	if (result.error) {
		            if (result.error.details && result.error.details[0].message) {
		                res.error(result.error.details[0].message);
		                return;
		            } else {
		                res.error(result.error.message);
		                return;
	            }
			}
			
			 let checkEmail = await UserModel.findOne({email}).lean(true)
        	

			 if(checkEmail) {
        		throw new Error('Email number already registered, please login')
            }

			let encryptedpassword = await bcrypt.hash(password, 10)

			let userData = { email, password: encryptedpassword, created_on ,name}

			let user = new UserModel(userData);
        	let userDetails = await user.save();
        	if(!userDetails) {
        		throw new Error('Unable to add details.')
			}

			delete userDetails._doc.password

        	res.Success(userDetails, "Signup successful.")
           
		} catch(error) {
			res.status(403).error(error.message);
		}
}



/*-------------------------------
+++++++++++++ user Login ++++++++++++
--------------------------------*/


exports.login = async(req, res) => {
	try {
		let {password, email} = req.body;
	  	const schema = Joi.object().keys({
			password: Joi.string().required().error(e => 'password require'),
			email: Joi.string().required().error(e => 'email require'),
    	})
    	const result = Joi.validate(req.body, schema, { abortEarly: true });
        	if (result.error) {
	            if (result.error.details && result.error.details[0].message) {
					res.error(result.error.details[0].message);
	                return;
	            } else {
					res.error(result.error.message);
	                return;
            }
		}

			let loginCredentials = await UserModel.findOne({email}).lean(true);

			if(!loginCredentials)
				return res.status(401).error('Invalid credentials')
	  

				let check = await bcrypt.compare(password, loginCredentials.password);
				if (!check) 
				  return res.status(401).error('Incorrect password')
		

				let access_token = jwt.sign({ id : loginCredentials._id }, 'supersecret' );

				let update = await UserModel.findOneAndUpdate({email},{access_token},{new:true})

				delete update._doc.password

				res.Success(update, "Login successful.")
    
	} catch(error) {
		res.status(403).error(error.message)
	}
}


/*-------------------------------
+++++++++++++ Add Rating ++++++++++++
--------------------------------*/


exports.addRatingAndComments = async(req, res) => {
	try {
		let {movieId, rate, comment } = req.body

		const schema = Joi.object().keys({
			movieId: Joi.string().required().error(e => 'movieId require'),
			rate: Joi.string().required().error(e => 'rate require'),
			comment :Joi.string().optional().allow(''),
		})
		const result = Joi.validate(req.body, schema, { abortEarly: true });
			if (result.error) {
				if (result.error.details && result.error.details[0].message) {
					res.error(result.error.details[0].message);
					return;
				} else {
					res.error(result.error.message);
					return;
			}
		}

			req.body.userId = req.userId

			let isAlready = await RateModel.findOne({userId : req.body.userId , movieId : movieId}).lean(true)

			if(isAlready) {
				throw new Error('Already Reviewed')
			}

			let rated = await RateModel.create(req.body)
			
			if(!rated) {
        		throw new Error('Something went wrong')
            }

			//Update rateCount in Movie Model 
			let getCount = (await RateModel.getRatingCount(movieId))[0]

			await MovieModel.findByIdAndUpdate(movieId,{avg_rate : getCount.avg_rate , total_rate : getCount.total_rate })

		    res.Success(rated, "Rated successfully.")

		} catch(error) {
			res.status(403).error(error.message)
		}
}



/*-------------------------------
+++++++++++++ get Movie Rating And Comments ++++++++++++
--------------------------------*/



exports.getRatingAndComments = async(req, res) => {
	try {
		let {movieId , limit = 0, page = 0} = req.body

		const schema = Joi.object().keys({
			movieId: Joi.string().required().error(e => 'movieId require'),
			page :Joi.string().optional().allow(''),
			limit :Joi.string().optional().allow(''),
		})
		const result = Joi.validate(req.body, schema, { abortEarly: true });
			if (result.error) {
				if (result.error.details && result.error.details[0].message) {
					res.error(result.error.details[0].message);
					return;
				} else {
					res.error(result.error.message);
					return;
			}
		}


			//Update rateCount in Movie Model 
			let AllRateAndComments = 	await RateModel.getRateAndComments({movieId},Number(limit) , Number(page) * Number(limit))


		    res.Success(AllRateAndComments, "")

		} catch(error) {
			res.status(403).error(error.message)
		}
}





exports.getMovies = async(req, res) => {
	try {
			let AllMoives = await MovieModel.find()
		    res.Success(AllMoives, "")

		} catch(error) {
			res.status(403).error(error.message)
		}
}



