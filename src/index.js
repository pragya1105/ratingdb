var express = require ("express");
var cors = require ("cors");
var bodyParser = require ("body-parser");
const app = express();
app.use(bodyParser.json());
app.use(cors());
const responseHandler = require('./Middlewares/responseHandler')
app.use('/', responseHandler);

app.use(bodyParser.urlencoded({
    extended: true
}));

require('./Routes/user').getRouter(app); 


const port = process.env.PORT || 3000;

app.listen(port);
console.log("Started on port " + port);