var{ mongoose, conn } = require("../Modules/connection");

let  userSchema  = mongoose.Schema(
    {
        name : String ,
        password: {
            type : String,
        },
        email : {
            type: String,
        },
        profileImage: {
            type: String,
            default: 'https://www.proadder.com/wp-content/uploads/2020/03/202-2026524_person-icon-default-user-icon-png.jpg'
        },
        access_token : String ,
     },
);
exports.UserModel = conn.model('User', userSchema); 
