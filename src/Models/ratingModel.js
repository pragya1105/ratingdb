var { mongoose, conn } = require("../Modules/connection");

let rateSchema = mongoose.Schema({

    userId:{     
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        default : null
    },

    movieId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'movie',
        default : null
    },
    rate: {
            type: Number,
            default: 0.0,
        },
    comment: {
            type: String,
            default: "N/A"
         },
    
    }, {
        timestamps: true,
    })


    rateSchema.statics.getRatingCount = async function (movieId) {
        return await this.aggregate([
            {
                $match : { movieId : mongoose.Types.ObjectId(movieId)  }
            } ,
            {
                $group: {
                    _id: "$movieId",
                    avg_rate: { $avg: "$rate" },
                    total_rate: { $sum: 1 }
                }
            }
        ])
    }


    rateSchema.statics.getRateAndComments = async function (query = {}, limit = 0, skip = 0) {
        limit = parseInt(limit);
        skip = parseInt(skip);
        return await this.find(query).limit(limit).skip(skip).sort({ rate : -1 }).populate('userId','name email profileImage')
    }


    exports.RateModel = conn.model('rate', rateSchema);
