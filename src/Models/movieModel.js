    var { mongoose, conn } = require("../Modules/connection");

    var movieSchema =  mongoose.Schema({
        name: {
            type: String,
        },
        url: {
            type: String,
        },
        avg_rate : {
            type : Number ,
            default : 0
        } ,
        total_rate : {
            type : Number ,
            default : 0
        } ,
    }, {
        timestamps:true
    })
    

    
     var MovieModel = conn.model('movie', movieSchema);
     exports.MovieModel = MovieModel
    
     MovieModel
    .findOne({})
    .then((result) => {
        if(!result){
            MovieModel.create({
                        name: "Harry Potter",
                        url : "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6e/Harry_Potter_wordmark.svg/1200px-Harry_Potter_wordmark.svg.png" ,
                    }, (err, done)=>{
                        if(err)
                            console.log("Error while create A admin");
                        else
                            console.log("Movie Created!");
                    })
        }
    }).catch((err) => {
            console.log("Movie error=====>>>>",err)
    });
   