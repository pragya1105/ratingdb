var user = require ('../Controllers/userController');
var auth = require ('../Modules/auth');

exports.getRouter = (app) => {

    app.route("/user/signup").post(user.signup);
    
    app.route("/user/login").post(user.login)
    app.route("/user/addRatingAndComments").post(auth.verifyToken ,user.addRatingAndComments)
    app.route("/user/getRatingAndComments").post(user.getRatingAndComments)

    app.route("/user/getMovies").get(user.getMovies)

    
}
